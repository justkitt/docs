﻿# Image classification

<a href="https://gitee.com/mindspore/docs/blob/master/lite/docs/source_en/image_classification.md" target="_blank"><img src="./_static/logo_source.png"></a>

## Image classification introduction

Image classification is to identity what an image represents, to predict the object list and the probabilites. For example，the following tabel shows the classification results after mode inference. 

![image_classification](images/image_classification_result.png)

| Category   | Probability |
| ---------- | ----------- |
| plant      | 0.9359      |
| flower     | 0.8641      |
| tree       | 0.8584      |
| houseplant | 0.7867      |

Using MindSpore Lite to realize image classification [example](https://gitee.com/mindspore/mindspore/tree/master/model_zoo/official/lite/image_classification).

## Image classification model list

The following table shows the data of some image classification models using MindSpore Lite inference.

> The performance of the table below is tested on the mate30.

| model name  | link | size | precision | CPU 4 thread delay |
|-----------------------|----------|----------|----------|-----------|
| MobileNetV2 |         |         |         |          |
| LeNet |          |         |         |          |
| AlexNet | |  |  |  |
| GoogleNet | |  |  |  |
| ResNext50 | |  |  |  |

